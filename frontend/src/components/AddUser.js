import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const AddUser = () => {
    const [name, setName] = useState("");
    const [sector, setSector] = useState("");
    const [agree, setAgree] = useState(false);
    const navigate = useNavigate();

    const handleChange = () => {
        setAgree(!agree);
    };

    const saveUser = async (e) => {
        e.preventDefault();
        try {
            await axios.post("http://localhost:5000/users", {
                name,
                sector,
                agree,
            });
            navigate("/");
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <div className="columns">
            <div className="column is-half">
                <form onSubmit={saveUser}>
                    <div className="field">
                        <label className="label">Name</label>
                        <div className="control">
                            <input
                                type="text"
                                className="input"
                                value={name || ""}
                                onChange={(e) => setName(e.target.value)}
                                placeholder="Name"
                            />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Sector</label>
                        <div className="control">
                            <div className="select is-fullwidth">
                                <select
                                    value={sector || ""}
                                    onChange={(e) => setSector(e.target.value)}
                                >
                                    <option value="">
                                        -- Choose Sector --
                                    </option>
                                    <option value="Technology">
                                        Technology
                                    </option>
                                    <option value="Finance">Finance</option>
                                    <option value="Healthcare">
                                        Healthcare
                                    </option>
                                    <option value="Education">Education</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Agree to terms</label>
                        <div className="control">
                            <input
                                type="checkbox"
                                checked={agree}
                                onChange={handleChange}
                            />{" "}
                            Tick if you agree to terms
                        </div>
                    </div>
                    <div className="field">
                        <div className="control">
                            <button type="submit" className="button is-success">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default AddUser;
